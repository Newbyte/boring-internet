# boring internet

boring internet is an opinionated extension for Firefox that makes the Internet
boring. Currently it boringifies following websites:

 - Old Reddit
 - YouTube
 - Deezer (very hacky and bad implementation at the moment)
 - Twitter (will probably break like tomorrow)

Boringification is done by removing frontpage recommendations on websites so
that when you visit them you're greeted with something boring rather than
engaging. Note that this removes functionality from websites and that this is by
design.

