const main_content = document.querySelector("[role=\"main\"]");

if (main_content !== null) {
	// We're on old reddit
	remove_children(main_content);
} else {
	// TODO: We're on new reddit -- implement boringifier!
}

function remove_children(parent_element) {
	while (parent_element.firstChild) {
		parent_element.firstChild.remove();
	}
}

