const main = document.getElementsByTagName("main")

const observer = new MutationObserver(function(mutations) {
	// I have no idea if this is stable, but I hope so. I have a feeling that
	// hope is nothing but a hope however.
	// Anyway, this is an attempt to match something that only appears once the
	// timeline has loaded, in this case I think this would be the first tweet
	// or something like that. Seems to work at least.
	if (mutations[0].addedNodes[0].childNodes[0].classList.toString() ===
		"css-1dbjc4n r-j7yic r-qklmqi r-1adg3ll r-1ny4l3l") {
			const timeline =
				document.querySelector("[aria-label=\"Timeline: Your Home Timeline\"]");

			while (timeline.firstChild) {
				timeline.firstChild.remove();
			}
		}
});

observer.observe(main[0], {
	childList: true,
	subtree: true
});
