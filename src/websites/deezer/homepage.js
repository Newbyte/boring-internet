document.addEventListener("click", remove_content);
remove_content();

// FIXME: Make this less of a horrible hack. For example, this will probably
// FIXME: fail on slower devices.
function remove_content() {
	if (/\/??\/$/.test(location.href)) {
		const existence_checker = setInterval(function() {
			console.log("checking ...");
			const page_content =
				document.getElementsByClassName("page-content");

			setTimeout(function() {
				if (page_content.length && page_content[0].firstChild) {
					clearInterval(existence_checker);

					const page_content_elem = page_content[0];

					while (page_content_elem.firstChild) {
						page_content_elem.firstChild.remove();
					}
				}
			}, 150);
		}, 150);
	}
}

